use crate::components::CurrentKeys;
use crate::components::IsShooting;
use specs::prelude::Entities;
use specs::prelude::Join;
use specs::prelude::Read;
use specs::ReadStorage;
use specs::WriteStorage;

pub struct FireKeyHandle;

impl<'a> specs::System<'a> for FireKeyHandle {
    type SystemData = (
        Entities<'a>,
        Read<'a, CurrentKeys>,
        ReadStorage<'a, crate::components::IsPlayer>,
        WriteStorage<'a, crate::components::TicksUntilReadyToFire>,
        WriteStorage<'a, crate::components::IsShooting>,
    );

    fn run(
        &mut self,
        (
            entities,
            current_keys_resource,
            is_player_storage,
            mut ticks_until_ready_to_fire_storage,
            mut is_shooting_storage,
        ): Self::SystemData,
    ) {
        for (entity, _is_player, ticks_until_ready_to_fire) in (
            &entities,
            &is_player_storage,
            &mut ticks_until_ready_to_fire_storage,
        )
            .join()
        {
            if ticks_until_ready_to_fire.ready_to_fire() && current_keys_resource.0.fire.is_down() {
                is_shooting_storage.insert(entity, IsShooting).unwrap();
            } else {
                match is_shooting_storage.remove(entity) {
                    _ => {}
                };
            }
        }
    }
}
