use crate::components::EntityType;
use crate::components::MoveDirection;
use crate::components::Position;
use crate::components::Velocity;
use specs::prelude::Join;
use specs::prelude::ReadStorage;
use specs::prelude::WriteStorage;
use specs::Entities;

pub struct UpdatePosition;

impl<'a> specs::System<'a> for UpdatePosition {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Velocity>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, EntityType>,
        ReadStorage<'a, MoveDirection>,
    );

    fn run(
        &mut self,
        (
            entities,
            velocity_storage,
            mut position_storage,
            entity_type_storage,
            move_direction_storage,
        ): Self::SystemData,
    ) {
        for (entity, velocity, position, entity_type, move_direction) in (
            &entities,
            &velocity_storage,
            &mut position_storage,
            &entity_type_storage,
            &move_direction_storage,
        )
            .join()
        {
            position.0 += move_direction.as_vector() * velocity.0;

            debug!(
                "{:?} {:?} {:?} {:?} ",
                entity, entity_type, move_direction, velocity,
            );
        }
    }
}
