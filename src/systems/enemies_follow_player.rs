use crate::components::EntityType;
use crate::components::IsPlayer;
use crate::components::MoveDirection;
use crate::components::Position;
use specs::prelude::Entities;
use specs::prelude::Join;
use specs::prelude::ReadStorage;
use specs::prelude::WriteStorage;

pub struct EnemiesFollowPlayer;

impl<'a> specs::System<'a> for EnemiesFollowPlayer {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, EntityType>,
        WriteStorage<'a, MoveDirection>,
        ReadStorage<'a, IsPlayer>,
    );

    fn run(
        &mut self,
        (
            entities,
            position_storage,
            entity_type_storage,
            mut move_direction_storage,
            is_player_storage,
        ): Self::SystemData,
    ) {
        let player_position_vec: Vec<&Position> = (&is_player_storage, &position_storage)
            .join()
            .map(|(_is_player, position)| position)
            .collect();

        for (entity, position, entity_type, move_direction) in (
            &entities,
            &position_storage,
            &entity_type_storage,
            &mut move_direction_storage,
        )
            .join()
        {
            match entity_type {
                EntityType::Kharedi | EntityType::_Aravi | EntityType::_Nekhe => {
                    for player_position in &player_position_vec {
                        let new_direction_vector = player_position.0 - position.0;
                        debug!("{:?}", new_direction_vector);
                        *move_direction = MoveDirection::from_vector(new_direction_vector);
                    }
                }
                _ => {}
            }

            debug!("{:?} {:?} {:?} ", entity, entity_type, move_direction,);
        }
    }
}
