use crate::components::EntityType;
use crate::components::MoveDirection;
use crate::components::Position;
use crate::components::TicksSinceLastEnemySpawn;
use crate::components::Velocity;
use crate::constants;
use crate::constants::MOVEMENT_SPEED;
use rand::Rng;
use specs::prelude::Entities;
use specs::prelude::Write;
use specs::WriteStorage;

pub struct SpawnEnemy;

impl<'a> specs::System<'a> for SpawnEnemy {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        Write<'a, TicksSinceLastEnemySpawn>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, EntityType>,
        WriteStorage<'a, MoveDirection>,
    );
    fn run(
        &mut self,
        (
            entities_read,
            mut ticks_since_last_enemy_spawn_resource,
            mut position_storage,
            mut velocity_storage,
            mut entity_type_storage,
            mut move_direction_storage,
        ): Self::SystemData,
    ) {
        let mut rng = rand::thread_rng();

        // Deal with time

        if ticks_since_last_enemy_spawn_resource.0 > 0 {
            ticks_since_last_enemy_spawn_resource.0 -= 1;
            return;
        }
        ticks_since_last_enemy_spawn_resource.0 = rng.gen_range(
            crate::constants::MINIMUM_SPAWN_TICKS,
            crate::constants::MAXIMUM_SPAWN_TICKS,
        );

        // Spawn enemy!
        {
            let new_enemy_position: Position = Position::new(
                rng.gen_range(0, constants::SCREEN_WIDTH) as f32,
                rng.gen_range(0, constants::SCREEN_HEIGHT) as f32,
            );
            let new_enemy_move_direction: MoveDirection = MoveDirection::new(1., 0.);
            let new_enemy_velocity = Velocity(MOVEMENT_SPEED);
            let new_enemy_entity = entities_read.create();
            let new_enemy_type = EntityType::Kharedi;

            position_storage
                .insert(new_enemy_entity, new_enemy_position)
                .unwrap();
            move_direction_storage
                .insert(new_enemy_entity, new_enemy_move_direction)
                .unwrap();
            velocity_storage
                .insert(new_enemy_entity, new_enemy_velocity)
                .unwrap();
            entity_type_storage
                .insert(new_enemy_entity, new_enemy_type)
                .unwrap();
        }
    }
}
