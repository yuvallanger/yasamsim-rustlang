//! ---- SPECS systems

use specs::RunNow;

mod count_time;
mod countdown_ticks_until_ready_to_fire;
mod countdown_to_destroy;
mod delete_hit_by_projectile;
mod delete_outbound_entities;
mod direction_keys_handle;
mod enemies_follow_player;
mod fire_key_handle;
mod print_values;
mod shoot_projectile;
mod spawn_enemy;
mod update_position;

pub use self::count_time::CountTime;
pub use self::countdown_ticks_until_ready_to_fire::CountdownTicksUntilReadyToFire;
pub use self::countdown_to_destroy::CountdownToDestroy;
pub use self::delete_hit_by_projectile::DeleteHitByProjectile;
pub use self::delete_outbound_entities::DeleteOutboundEntities;
pub use self::direction_keys_handle::DirectionKeysHandle;
pub use self::enemies_follow_player::EnemiesFollowPlayer;
pub use self::fire_key_handle::FireKeyHandle;
pub use self::print_values::PrintValues;
pub use self::shoot_projectile::ShootProjectile;
pub use self::spawn_enemy::SpawnEnemy;
pub use self::update_position::UpdatePosition;

pub fn make_specs_dispatcher() -> specs::Dispatcher<'static, 'static> {
    specs::DispatcherBuilder::new()
        .with(
            CountdownTicksUntilReadyToFire,
            "countdown_ticks_until_ready_to_fire",
            &[],
        )
        .with(CountdownToDestroy, "countdown_to_destroy", &[])
        .with(DirectionKeysHandle, "direction_keys_handle", &[])
        .with(FireKeyHandle, "fire_key_handle", &["direction_keys_handle"])
        .with(
            PrintValues {
                stage: "before_update",
            },
            "print_values_before_update",
            &[],
        )
        .with(
            ShootProjectile,
            "entity_shoots_projectile",
            &[
                "countdown_ticks_until_ready_to_fire",
                "direction_keys_handle",
                "fire_key_handle",
            ],
        )
        .with(
            UpdatePosition,
            "update_position",
            &["entity_shoots_projectile"],
        )
        .with(
            PrintValues {
                stage: "after_update",
            },
            "print_values_after_update",
            &["update_position"],
        )
        .with(SpawnEnemy, "system_spawn_enemy", &[])
        .with(DeleteOutboundEntities, "delete_outbound_entities", &[])
        .with(DeleteHitByProjectile, "delete_hit_by_projectile", &[])
        .build()
}

pub fn run_now(world: &mut specs::World) {
    let mut countdown_ticks_until_ready_to_fire = CountdownTicksUntilReadyToFire;
    let mut countdown_to_destroy = CountdownToDestroy;
    let mut direction_keys_handle = DirectionKeysHandle;
    let mut enemies_follow_player = EnemiesFollowPlayer;
    let mut fire_key_handle = FireKeyHandle;
    let mut print_values_before_update = PrintValues {
        stage: "before_update",
    };
    let mut shoot_projectile = ShootProjectile;
    let mut update_position = UpdatePosition;
    let mut print_values_after_update = PrintValues {
        stage: "after_update",
    };
    let mut system_spawn_enemy = SpawnEnemy;
    let mut delete_outbound_entities = DeleteOutboundEntities;
    let mut delete_hit_by_projectile = DeleteHitByProjectile;
    let mut count_time = CountTime;

    countdown_ticks_until_ready_to_fire.run_now(&world.res);
    countdown_to_destroy.run_now(&world.res);
    direction_keys_handle.run_now(&world.res);
    enemies_follow_player.run_now(&world.res);
    fire_key_handle.run_now(&world.res);
    print_values_before_update.run_now(&world.res);
    shoot_projectile.run_now(&world.res);
    update_position.run_now(&world.res);
    print_values_after_update.run_now(&world.res);
    system_spawn_enemy.run_now(&world.res);
    delete_outbound_entities.run_now(&world.res);
    delete_hit_by_projectile.run_now(&world.res);
    count_time.run_now(&world.res);

    world.maintain();
}
