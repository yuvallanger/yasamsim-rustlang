use crate::components::EntityType;
use crate::components::IsShooting;
use crate::components::MoveDirection;
use crate::components::Position;
use crate::components::ShootingDirection;
use crate::components::TicksUntilDestroyed;
use crate::components::TicksUntilReadyToFire;
use crate::components::Velocity;
use crate::constants;
use specs::prelude::Entities;
use specs::prelude::Entity;
use specs::prelude::Join;
use specs::ReadStorage;
use specs::WriteStorage;

pub struct ShootProjectile;

impl<'a> specs::System<'a> for ShootProjectile {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, ShootingDirection>,
        ReadStorage<'a, IsShooting>,
        WriteStorage<'a, TicksUntilReadyToFire>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, TicksUntilDestroyed>,
        WriteStorage<'a, EntityType>,
        WriteStorage<'a, MoveDirection>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut position_storage,
            shoot_direction_storage,
            is_shooting_storage,
            mut ticks_until_ready_to_fire_storage,
            mut velocity_storage,
            mut ticks_until_destroyed_storage,
            mut entity_type_storage,
            mut move_direction_storage,
        ): Self::SystemData,
    ) {
        let mut new_projectile_vec: Vec<(Entity, Position, ShootingDirection)> = Vec::new();

        for (_is_shooting, position, shoot_direction, ticks_until_ready_to_fire) in (
            &is_shooting_storage,
            &mut position_storage,
            &shoot_direction_storage,
            &mut ticks_until_ready_to_fire_storage,
        )
            .join()
        {
            if ticks_until_ready_to_fire.ready_to_fire() {
                let new_projectile_entity = entities.create();
                let new_projectile_position = position;
                let new_projectile_direction: ShootingDirection = *shoot_direction;

                new_projectile_vec.push((
                    new_projectile_entity,
                    *new_projectile_position,
                    new_projectile_direction,
                ));

                ticks_until_ready_to_fire.reset_with(constants::TICKS_UNTIL_READY_TO_FIRE);
            };
        }

        for (new_projectile_entity, new_projectile_position, new_projectile_direction) in
            new_projectile_vec
        {
            position_storage
                .insert(new_projectile_entity, new_projectile_position)
                .unwrap();

            move_direction_storage
                .insert(
                    new_projectile_entity,
                    MoveDirection::from_vector(new_projectile_direction.as_vector()),
                )
                .unwrap();

            velocity_storage
                .insert(new_projectile_entity, Velocity(constants::PROJECTILE_SPEED))
                .unwrap();

            ticks_until_destroyed_storage
                .insert(new_projectile_entity, TicksUntilDestroyed(100))
                .unwrap();

            entity_type_storage
                .insert(new_projectile_entity, EntityType::Projectile)
                .unwrap();
        }
    }
}
