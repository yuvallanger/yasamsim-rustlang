use crate::components::FaceDirection;
use crate::components::Position;
use crate::components::TicksSinceStart;
use crate::constants;
use crate::constants::TAU32;
use nalgebra::Vector2;
use ncollide2d::bounding_volume::AABB;
use quicksilver::prelude::Col;
use quicksilver::prelude::Color;
use quicksilver::prelude::Rectangle;
use quicksilver::prelude::Vector;
use quicksilver::prelude::Window;

fn _draw_pentagram(
    x: f32,
    y: f32,
    pentagram_size: f32,
    spin: f32,
    window: &mut Window,
) {
    let a = (0..5)
        .map(|n| n as f32 * (TAU32 + spin) / 5.)
        .map(|n| Vector::new(f32::sin(n) + x, f32::cos(n) + y) * pentagram_size);

    let a2 = a.clone();

    let b = a.cycle().skip(2).zip(a2).map(|(v1, v2)| (v1, v2));

    for (v1, v2) in b {
        window.draw(&quicksilver::geom::Line::new(v1, v2), Col(Color::RED));
    }
}

pub fn draw_facing_rectangle(
    window: &mut Window,
    position: Position,
    face_direction: FaceDirection,
    half_width: f32,
    half_height: f32,
    color: Color,
    ticks_since_start: TicksSinceStart,
) {
    draw_rectangle(window, position, half_width, half_height, color);

    let face_rectangle_offset_size = 20.;
    let face_rectangle_offset = face_direction.as_vector() * face_rectangle_offset_size;

    let jiggle_period = 10;
    let jiggle_offset_size = 10.;
    let jiggle_wave = counter_to_wave(ticks_since_start.0, jiggle_period) * jiggle_offset_size;
    let jiggle_offset = face_direction
        .as_vector()
        .yx()
        .component_mul(&Vector2::new(jiggle_wave, -jiggle_wave));

    let face_square_vector = position.0 + face_rectangle_offset + jiggle_offset;
    let face_square_position = Position(face_square_vector);

    draw_rectangle(
        window,
        face_square_position,
        constants::PROJECTILE_HALF_HEIGHT,
        constants::PROJECTILE_HALF_HEIGHT,
        Color::BLACK,
    );
}

fn counter_to_wave(
    counter: i64,
    period: i64,
) -> f32 {
    (((counter % period) as f32) * constants::TAU32 / period as f32).sin()
}

pub fn draw_rectangle(
    window: &mut Window,
    position: Position,
    half_width: f32,
    half_height: f32,
    color: Color,
) {
    let whoknowswhat_rectangle = Rectangle::from(AABB::from_half_extents(
        position.0,
        Vector2::new(half_width, half_height),
    ));

    window.draw(&whoknowswhat_rectangle, color);
}
