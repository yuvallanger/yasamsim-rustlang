use specs::Component;
use specs::VecStorage;

#[derive(Clone, Copy, Debug, Default)]
/// TicksUntilReadyToFire is used to keep track of time in units of ticks
/// until next time an entity is ready to fire, not the time it will actually
/// fire.
pub struct TicksUntilReadyToFire(pub i64);

impl TicksUntilReadyToFire {
    pub fn count_down(&mut self) {
        self.0 -= 1;
    }

    pub fn reset_with(
        &mut self,
        ticks: i64,
    ) {
        self.0 = ticks;
    }

    pub fn ready_to_fire(self) -> bool {
        self.0 <= 0
    }
}
impl Component for TicksUntilReadyToFire {
    type Storage = VecStorage<Self>;
}
