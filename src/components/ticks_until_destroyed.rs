use specs::Component;
use specs::VecStorage;

#[derive(Clone, Copy)]
pub struct TicksUntilDestroyed(pub i64);

impl TicksUntilDestroyed {
    pub fn is_destroyable(self) -> bool {
        self.0 <= 0
    }
}
impl Component for TicksUntilDestroyed {
    type Storage = VecStorage<Self>;
}
