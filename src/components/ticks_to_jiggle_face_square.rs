use specs::prelude::Component;
use specs::prelude::VecStorage;

#[derive(Clone, Copy, Default)]
pub struct TicksToJiggleFaceSquare(pub i32);

impl Component for TicksToJiggleFaceSquare {
    type Storage = VecStorage<Self>;
}
