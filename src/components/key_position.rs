#[derive(Clone, Copy, Debug, PartialEq)]
pub enum KeyPosition {
    Up,
    Down,
}
impl KeyPosition {
    pub fn from_is_down(is_down: bool) -> KeyPosition {
        if is_down {
            KeyPosition::Down
        } else {
            KeyPosition::Up
        }
    }
    pub fn is_down(self) -> bool {
        match self {
            KeyPosition::Down => true,
            KeyPosition::Up => false,
        }
    }
}
impl Default for KeyPosition {
    fn default() -> Self {
        KeyPosition::Up
    }
}
