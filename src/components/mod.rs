//! ---- SPECS components and resources.

mod entity_type;
mod face_direction;
mod health_points;
mod is_player;
mod is_shooting;
mod key_position;
mod keys;
mod move_direction;
mod position;
mod resources;
mod shooting_direction;
mod ticks_to_jiggle_face_square;
mod ticks_until_destroyed;
mod ticks_until_ready_to_fire;
mod velocity;

pub use self::entity_type::EntityType;
pub use self::face_direction::FaceDirection;
pub use self::health_points::HealthPoints;
pub use self::is_player::IsPlayer;
pub use self::is_shooting::IsShooting;
pub use self::key_position::KeyPosition;
pub use self::keys::Keys;
pub use self::move_direction::MoveDirection;
pub use self::position::Position;
pub use self::resources::CurrentKeys;
pub use self::resources::PreviousKeys;
pub use self::resources::TicksSinceLastEnemySpawn;
pub use self::resources::TicksSinceStart;
pub use self::shooting_direction::ShootingDirection;
pub use self::ticks_to_jiggle_face_square::TicksToJiggleFaceSquare;
pub use self::ticks_until_destroyed::TicksUntilDestroyed;
pub use self::ticks_until_ready_to_fire::TicksUntilReadyToFire;
pub use self::velocity::Velocity;
