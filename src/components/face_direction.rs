use nalgebra::Vector2;
use specs::Component;
use specs::VecStorage;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct FaceDirection(Vector2<f32>);

impl FaceDirection {
    pub fn new(
        x: f32,
        y: f32,
    ) -> Self {
        FaceDirection::from_vector(Vector2::new(x, y))
    }

    pub fn from_vector(v: Vector2<f32>) -> Self {
        let zv = Vector2::new(0., 0.);
        if v == zv {
            FaceDirection(zv)
        } else {
            FaceDirection(v.normalize())
        }
    }

    pub fn as_vector(self) -> Vector2<f32> {
        let FaceDirection(v) = self;
        v
    }
}
impl Default for FaceDirection {
    fn default() -> FaceDirection {
        FaceDirection::new(0., 0.)
    }
}
impl Component for FaceDirection {
    type Storage = VecStorage<Self>;
}
