use specs::Component;
use specs::VecStorage;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EntityType {
    Yasam,
    Kharedi,
    _Aravi,
    _Nekhe,
    Projectile,
}

impl Default for EntityType {
    fn default() -> EntityType {
        EntityType::Yasam
    }
}
impl Component for EntityType {
    type Storage = VecStorage<Self>;
}
