use crate::components::keys::Keys;

#[derive(Clone, Copy, Debug, Default)]
pub struct TicksSinceLastEnemySpawn(pub i64);
#[derive(Clone, Copy, Debug, Default)]
pub struct TicksSinceStart(pub i64);
#[derive(Clone, Copy, Debug, Default)]
pub struct PreviousKeys(pub Keys);
#[derive(Clone, Copy, Debug, Default)]
pub struct CurrentKeys(pub Keys);
