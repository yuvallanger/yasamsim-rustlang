## 2019-06-11

TODO: Learn how to use ncollide2d's BoundingVolume trait.

### Compiling with cargo-web

You CANNOT use:

* `std::time::Instant::now()`

You MUST use:

* `shred` with `features = ["nightly"]`, hence:
* rust nightly…
* a combo of the crates: `log` `web_logger` etc. instead of `println!`!

## Earlier

* 2018-12-18T21:07:36+0000 < Ralith> cow_2001: so yeah, you want to 1. pick a seriralizable RNG, and 2. use a master RNG to generate seeds for child RNGs that can be used in parallel

